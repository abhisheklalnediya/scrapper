from django.db import models
from django.contrib.auth.models import User

class Scrapper(models.Model):
	user = models.ForeignKey(User)
	url = models.TextField()
	def __unicode__(self):
		return str(self.user) + ' : ' + str(self.url)[0:30]
	
class Scrappkeys(models.Model):
	scrapper = models.ForeignKey(Scrapper)
	title = models.TextField()
	sampledata1 = models.TextField()
	sampledata2 = models.TextField()
	isurl = models.BooleanField(default=False)
	isSubofCol1 =  models.BooleanField(default=False)
	colid = models.IntegerField()
	data_position = models.TextField(null=True)
	def __unicode__(self):
		return str(self.scrapper) + ' : ' + str(self.title)[0:30]
		
#class pagination (models.Model):
	
class Scrappeddata(models.Model):
	scrapper = models.ForeignKey(Scrapper)
	scrapperkey = models.ForeignKey(Scrappkeys, null=True)
	#pagination = models.ForeignKey(pagination)
	
	data = models.TextField(blank=True)
	
	def __unicode__(self):
		return str(self.scrapper) + ' : ' + str(self.data)[0:30]
	
	
class Jobs(models.Model):
	scrapper = models.ForeignKey(Scrapper)
	status = models.CharField(max_length=2, default='0',choices=(('0','Queued'),('1','Complete'),('2','In progress'),('3','Error'),('4','Waiting')))
	time_Added = models.DateTimeField(auto_now=True)
	time_last_updated = models.DateTimeField(auto_now=True,null=True)
	key = models.TextField(null=True)
	#url = models.TextField(null=True)
	#output = models.CharField(max_length=1, default=-1,choices=(('0','JSON'),('1','Xls'),('2','csv'),))
	#pagination = models.ForeignKey(pagination)
	#priority = models.CharField(max_length=1, default=-1, choices=(('h','High'),), verbose_name='Birth')
	def __unicode__(self):
		return str(self.scrapper) + ' : ' + str(self.status)[0:30]