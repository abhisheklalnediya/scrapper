from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django import http
import re
from django.http import HttpResponseRedirect
from thescrapper import *
import urllib2
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.http import Http404
from django.template.loader import render_to_string
import simplejson
from scrapper.models import *
from scrapper.forms import *
from django.contrib.auth.models import User
import uuid
import hashlib
import time
import threading
from django.template import loader, Context
import numpy as np
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.core.mail import mail_admins
from django.core.mail import EmailMultiAlternatives
def welcomeMail(request, subject = "Thank you for trying Scrapper.TrrTrr.Biz."):
	try:
		template_html = 'mail/CanIhelpyou.html'
		template_text = 'mail/CanIhelpyou.txt'
		text_content = render_to_string(template_text,context_instance=RequestContext(request))
		html_content = render_to_string(template_html,context_instance=RequestContext(request))
		msg = EmailMultiAlternatives(subject, text_content,'doovispl@gmail.com', ['doovispl@gmail.com',request.user.email])
		msg.attach_alternative(html_content, "text/html")
		msg.content_subtype = "html"
		msg.send()
	except:
		pass
	
def index(request):
	#message = "From : Tesst"
	#mail_admins("TEST", message)
	return render_to_response("home.html", context_instance=RequestContext(request))

@login_required
def addNewScrapper1(request):
	welcomeMail(request)
	return render_to_response("addscrapper1.html", context_instance=RequestContext(request))

@login_required
def addNewScrapper2(request):
	if request.method != 'POST' and not ('url' in request.POST):
		return HttpResponseRedirect("/")
	url = request.POST['url']
	responce={}
	responce['url'] = url
	#noofcols = request.POST['noofcols']
	val = URLValidator()
	try:
		val(url)
	except ValidationError, e:
		print "Error, url"
		responce['urlerror'] = "The url is valid. :("
		responce['url'] = url
		return render_to_response("addscrapper1.html",responce, context_instance=RequestContext(request))

	print url
	responce['url'] = url

	htmlRsponce = urllib2.urlopen(url)
	meta = htmlRsponce.info()
	contlength=0
	try:
		contlength=meta.getheaders("Content-Length")[0]
	except:
		pass

	print "Content-Length:", contlength
	print "Content-Type:", meta.getheaders("Content-Type")[0]
	if not "text/html" in meta.getheaders("Content-Type")[0]:
		print "Error, not html"
		responce['urlerror'] = "The url is not pointing to a html page. :("
		return render_to_response("addscrapper1.html",responce, context_instance=RequestContext(request))
	elif int() > 300000:
		print "Too Large page"
		responce['urlerror'] = "Phew! Its a huge page :( . Please contact us for custom scrappers! :)"
		return render_to_response("addscrapper1.html",responce, context_instance=RequestContext(request))

	#inpStr1 = "How to parse a kml file to csv using python/BeautifulSoup or similar?"
	#inpStr2 = "how to add lists to a dictionary then output to .csv"
	#url1 = "http://stackoverflow.com/questions/tagged/beautifulsoup?page=2&sort=newest&pagesize=15"
	#url2 = "http://stackoverflow.com/questions/tagged/beautifulsoup?page=133&sort=newest&pagesize=15"
	#count = 3

	#scrapPage('https://www.airbnb.com/s/London?neighborhoods%5B%5D=The+West+End','Central London - Oxford St & Soho','DOUBLE ROOM in Heart of London|SOHO')

	scrapper_db = Scrapper(url=url,user=request.user)
	scrapinst = scrapper_db.save()
	responce['scrapperID'] = scrapper_db.id
	print responce['scrapperID']
	return render_to_response("addscrapper2.html", responce, context_instance=RequestContext(request))

@login_required
def verifycolumn(request):
	if request.method != 'POST':
		raise Http404
	print request.POST['url']
	print request.POST['colid']
	url = request.POST['url']
	colid = int(request.POST['colid'])
	scrapid = request.POST['scrapid']
	sampledata1 = request.POST['sampledata1']
	sampledata2 = request.POST['sampledata2']
	title = request.POST['title']
	#print str(scrapPage(url,sampledata1,sampledata2))
	responce={}
	responce['title'] = request.POST['title']
	responce['status'] = 'success'

	try:
		scrapdata=None
		if colid == 1:
			scrapdata = scrapPage(url,sampledata1,sampledata2)
			responce['scrapedlist'] = scrapdata['pages'][0]
			print 'Primary col complete'
		elif colid > 1 :
			try:
				scrapkey_db = Scrappkeys.objects.get(scrapper = Scrapper.objects.get(pk=scrapid), colid=1)
			except Scrappkeys.DoesNotExist:
				scrapkey_db = None
				raise Exception("TRRErr : Please Verify column 1")
			scrapdata = scrapPage(url,sampledata1,sampledata2,scrapperid=scrapid,colid=colid)
			responce['scrapedlist'] = scrapdata['pages'][0]
			print scrapdata['pages']
			print 'col complete'
		else :
			print 'I got a fish'
			raise Exception(":)?")
		print responce['scrapedlist']
		responce['data'] = render_to_string("addcolumnlist.html",responce,context_instance=RequestContext(request))
		scrapkey_db, created = Scrappkeys.objects.get_or_create(scrapper = Scrapper.objects.get(pk=scrapid), colid=colid)
		scrapkey_db.sampledata1 = sampledata1
		scrapkey_db.sampledata2 = sampledata2
		scrapkey_db.title = title
		scrapkey_db.data_position = str(scrapdata['pos'])[1:-1]
		scrapkey_db.save()
	except Exception,e:
		emessage = ""
		if str(e)[0:6] == "TRRErr" :
			emessage = str(e)[8:]
		responce['status'] = 'Failed'
		responce['message'] = '<strong>Oops!</strong>The Sample data seems to be in valid!' + emessage
		welcomeMail(request,subject="Trouble using Scrapper, Let me Help You! url : " + url )
		
	to_json = []
	to_json.append(responce)
	final_responce = simplejson.dumps(to_json)
	return HttpResponse(final_responce, mimetype='application/json')
	# convert the list to JSON
	#return render_to_response("addcolumnlist.html",responce,context_instance=RequestContext(request))

@login_required
def saveAndAddJob(request):
	if request.method != 'POST':
		raise Http404
	responce = {}
	scrapid = request.POST['scrapid']
	noofcols = int(request.POST['noofCols'])
	noofkey = Scrappkeys.objects.filter(scrapper = Scrapper.objects.get(pk=scrapid)).count()
	if noofcols != noofkey and (noofcols > 0):
		responce['status'] = 'Failed'
		responce['Message'] = 'Please Verify all coumns and ensure data is scrappable. Number of Colums Selected : ' + 	str(noofcols)
	else:

		job, created = Jobs.objects.get_or_create(scrapper = Scrapper.objects.get(pk=scrapid))
		h = hashlib.new('ripemd160')
		h.update(str(request.user))
		job.key = str(uuid.uuid4()) + str(h.hexdigest())
		job.save()

		#jobProcess = Process(target=scrapJob, args=(job,))

		jobThread = threading.Thread(target=scrapJob, args = (job,))
		jobThread.daemon = True
		jobThread.start()
		#scrapJob(job)
		responce['status'] = 'Success'

	to_json = []
	to_json.append(responce)
	final_responce = simplejson.dumps(to_json)

	return HttpResponse(final_responce, mimetype='application/json')

def scrapJob(job, sleeptime = 10):
	print "JOB : "
	url = job.scrapper.url
	print url
	scrapperkeys = Scrappkeys.objects.filter(scrapper = job.scrapper)

	pageData=None
	subdata=None
	scrapperid=None
	for sk in scrapperkeys:
		job.status = 2
		job.save()
		scrapperid = sk.scrapper.pk
		print scrapperid 
		print sk.colid
		scrapdata = scrapPage(url,sk.sampledata1,sk.sampledata2,subdata,pageData,scrapperid=scrapperid, colid=sk.colid)
		
		pageData=scrapdata['htmlText']
		subdata=True
		for data in scrapdata['pages'][0]:
			sd_db = Scrappeddata()
			sd_db.scrapper = job.scrapper
			sd_db.scrapperkey = sk
			sd_db.data = str(data)
			sd_db.save()
	#job.status = 4
	#job.save()
	time.sleep(5)
	job.status = 1
	job.save()

@login_required
def myScrappers(request):
	jobs = Jobs.objects.filter(scrapper__user=request.user)
	#print dir(jobs[0].status)
	responce = {'jobs':jobs}
	return render_to_response("myscrappers.html",responce,context_instance=RequestContext(request))

def ScrappedDataOutput(request,key,OpType):
	responce = {}

	job=Jobs.objects.get(key=key)
	heads = Scrappkeys.objects.values_list('title', flat=True).filter(scrapper=job.scrapper)
	responce['heads']=heads
	heads = Scrappkeys.objects.filter(scrapper=job.scrapper)
	datacol=[]
	for h in heads:
		col = Scrappeddata.objects.filter(scrapperkey=h)
		datacol.append(col)

	x = np.array(datacol)
	responce['datacol']=x.T
	print dict(responce)
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="Output.csv"'


	if OpType == 'CSV':
		t = loader.get_template("output/csv")
		c = Context({
			'datacol': responce['datacol'],
			'heads': heads,
		})
		response.write(t.render(c))
		return response
	elif OpType == 'JSON':
		jsdict={}
		dci=0
		for dc in responce['datacol']:
			dii=0
			jsdict[dci]={}
			for di in dc:
				jsdict[dci][str(heads[dii].title)]=di.data
				dii+=1
			dci+=1
		to_json = []
		to_json.append(jsdict)
		final_responce = simplejson.dumps(to_json)
		return HttpResponse(final_responce, mimetype='application/json')
	else:
		raise Http404

def jscript (request):
	return render_to_response("script.js",context_instance=RequestContext(request))


def newUser (request):
	user_form = UserCreateForm(request.POST)
	if user_form.is_valid():
		username = user_form.clean_username()
		password = user_form.clean_password2()
		user_form.save()
		user = authenticate(username=username,password=password)
		login(request, user)
		return HttpResponseRedirect("/")

	if request.method != 'POST':
		user_form = UserCreateForm()

	return render_to_response("registration/newUser.html",{ 'form' : user_form },context_instance=RequestContext(request))

def contactus(request):
	responce={}
	if request.method == 'POST' and 'message' in request.POST:
		if request.POST['subject'] == "" :
			responce['reply']="Subject is Required"
		elif request.POST['message'] == "" :
			responce['reply']="Message is Required"
		elif request.POST['email'] == "" :
			responce['reply']="Email is Required"
		else:
			try:
				message = "From : " + request.POST['email'] + "\n\n" + request.POST['message']
				mail_admins(request.POST['subject'], message)
				responce['reply']="Thank You! We will Get back to you Soon!"
			except:
				responce['reply']="Oops! Message Send Failed"
	return render_to_response("registration/messageme.html",responce,context_instance=RequestContext(request))
