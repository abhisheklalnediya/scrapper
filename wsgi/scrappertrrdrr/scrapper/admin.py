from django.contrib import admin
from scrapper.models import *

admin.site.register(Scrapper)
admin.site.register(Scrappeddata)
admin.site.register(Scrappkeys)
admin.site.register(Jobs)
