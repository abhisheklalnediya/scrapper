import urllib2
from bs4 import BeautifulSoup
import re
from time import sleep
import difflib
from scrapper.models import *

def generatePagelinks(url1,url2,pagecount):
	matcher = difflib.SequenceMatcher(a=url1, b=url2)
	matches = matcher.get_matching_blocks()

	prefix = url1[:matches[0][2]]
	suffix = url2[matches[1][1]:]
	urls = []
	for i in range(2, pagecount + 1):
		urls.append( prefix + str(i) + suffix )
	return urls

def is_Childe(parent,child):
	i=0
	for c in parent.children:
		#print c
		#print "*****************************************************************"
		#print child
		#print "*****************************************************************"
		
		if c == child :
			#print c.prettify()
			#print "Success"
			return i+1
		i+=1
	#print "Oops No relation"
	return False

def printSibils(parent1,childpos1):
	result=[]
	for sibli in parent1.contents:
		childSel = sibli
		childerror = False
		for i in reversed(childpos1):
			#print child1.contents[i - 1]
			try:
				childSel = childSel.contents[i - 1]
			except:
				childerror = True
				break
		if not childerror:
			print childSel
			result.append(childSel)
		else:
			#result.append('')
			pass

	return result

def scrapPage(url,inpStr1,inpStr2,subdata=None,pageData=None,scrapperid=None, colid=1, pageurl1=None,pageurl2=None,pagecount=0):
	htmlTest=None
	if subdata and pageData:
		htmlTest=pageData
	else:
		htmlRsponce = urllib2.urlopen(url)
		htmlTest = htmlRsponce.read()
		
	if scrapperid and colid > 1:
		subinpstring1 = inpStr1
		subinpstring2 = inpStr2
		scrapkey_db = Scrappkeys.objects.get(scrapper = Scrapper.objects.get(pk=scrapperid), colid=1)
		inpStr1 = scrapkey_db.sampledata1
		inpStr2 = scrapkey_db.sampledata2
		
	data  = {}
	data['htmlText']=htmlTest
	#print htmlTest

	soup = BeautifulSoup(htmlTest)

	inp1 = soup.find(text=re.compile(r'\s*%s.*' % inpStr1))
	print inp1
	inp2 = soup.find(text=re.compile(r'\s*%s\s*' % inpStr2))
	print inp2

	
		
	mainchild1 = inp1
	mainchild2 = inp2
	childpos1 = []
	childpos2 = []

	level = 0

	while not is_Childe(mainchild1.find_parent(),mainchild2):
		childpos1.append(is_Childe(mainchild1.find_parent(),mainchild1))
		childpos2.append(is_Childe(mainchild2.find_parent(),mainchild2))

		mainchild1 = mainchild1.find_parent()
		mainchild2 = mainchild2.find_parent()

		#print "level " + str(level)
		level += 1

	#print str(is_Childe(mainchild1.find_parent(),mainchild1)) + "th Child"

	print childpos1
	print childpos2


	if childpos1 != childpos2 :
		print "Position Error"


	parent1=mainchild1.find_parent()
	parent2=mainchild2.find_parent()
	subchildpos1 = []
	if scrapperid  and colid > 1:
		subinp1 = parent1.find(text=re.compile(r'\s*%s.*' % subinpstring1))
		submainchild=subinp1
		
		print "subinput1"
		print subinp1
		while parent1 != submainchild:
			subchildpos1.append(is_Childe(submainchild.find_parent(),submainchild))
			#childpos2.append(is_Childe(mainchild2.find_parent(),mainchild2))
			#print submainchild
			submainchild = submainchild.find_parent()
			#mainchild2 = mainchild2.find_parent()
			
			#print "level " + str(level)
			level += 1
		#print subchildpos1[1:]
		#print parent1
		#print printSibils(parent1,subchildpos1[:-1])
		#print "pos abv"
	#print mainchild1.find_parent()['id']
	#print mainchild1.find_parent()['class']
	child1=parent1
	child2=parent2
	page=[]
	if scrapperid and colid > 1:
		page.append(printSibils(parent1,subchildpos1[:-1]))
		data['pos'] = subchildpos1
	else:
		page.append(printSibils(parent1,childpos1))
		data['pos'] = childpos1

	data['pages'] = page
	

	if pagecount > 0 and (pageurl1 and pageurl2):
		for url in generatePagelinks(pageurl1,pageurl2,pagecount):
			print url
			htmlRsponce = urllib2.urlopen(url)
			htmlTest = htmlRsponce.read()
			soup = BeautifulSoup(htmlTest)
			pageFriend=None
			if len(soup.find_all(id=mainchild1.find_parent()['id'])) == 1:
				pageFriend="id"
				print "pageFriend=id"
				printSibils(soup.find(id=mainchild1.find_parent()['id']),childpos1)
			elif len(soup.find_all(id=mainchild1.find_parent()['class'][0])) == 1:
				pageFriend="class"
				print "pageFriend=class"
	return data


















#url = "http://stackoverflow.com/questions/tagged/beautifulsoup"
#inpStr1 = "Finding text inside link thats visible"
#inpStr2 = "Beautiful Soup decode error"
#url1 = "http://stackoverflow.com/questions/tagged/beautifulsoup?page=2&sort=newest&pagesize=15"
#url2 = "http://stackoverflow.com/questions/tagged/beautifulsoup?page=133&sort=newest&pagesize=15"
#count = 3

#scrapPage(url,inpStr1,inpStr2,url1,url2,count)
