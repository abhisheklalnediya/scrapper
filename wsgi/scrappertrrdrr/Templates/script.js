var $slider = $("#ColNoSlider");
if ($slider.length) {
    $slider.slider({
        min: 1,
        max: 9,
        value: 2,
        orientation: "horizontal",
        range: "min",
        change: function(event, ui) { 
            $('#ColNoSliderbdg').html(ui.value);
            $("#ColNoSliderinp").val(ui.value);
            for (i=2;i<10;i++)
            {
                if(ui.value<i)
                {
                    $('div[column="'+i+'"]').slideUp();
                }
                else
                {
                    $('div[column="'+i+'"]').slideDown();
                }
            
            }
        
        }
    }).addSliderSegments($slider.slider("option").max);
}

$(".verifybtn").click(function(){
    var url = $('#mainurl').val();
    var scrapid = $('#scrapid').val();
    
    var verifybtn = $(this);
    
    $(this).parents('.columndef').find('input[name="Title"],input[name="sampledata1"],input[name="sampledata2"]').filter(function() {
        return this.value != "";
    }).parents('.form-group').removeClass('has-error').addClass('has-success');
    if ( $(this).parents('.columndef').find('input[name="Title"],input[name="sampledata1"],input[name="sampledata2"]').filter(function() {
        return this.value == "";
    }).parents('.form-group').removeClass('has-success').addClass('has-error').length)
    {
        alert("oops");
        return;
    }
    
    var title = $(this).parents('.columndef').find('input[name="Title"]').val();
    var sampledata1 = $(this).parents('.columndef').find('input[name="sampledata1"]').val();
    var sampledata2 = $(this).parents('.columndef').find('input[name="sampledata2"]').val();
    if( ! sampledata2)
    {
        sampledata2=sampledata1
    }
    var colummndef = $(this).parents('.columndef');
    $(verifybtn).addClass('disabled');
    $(colummndef).find('.message-box').slideUp();
    $.post( "/verifycolumn", {
        url: url,
        title: title,
        sampledata1 : sampledata1,
        sampledata2 : sampledata2,
        csrfmiddlewaretoken: "{{csrf_token}}",
        scrapperid : '',
        colid : $(colummndef).attr("column"),
        scrapid : scrapid
    })
    .done(function( data ) {
        $(colummndef).find('.sampleresult').slideUp();
        if(data[0].status=='success')
        {   
            $(colummndef).find('.sampleresult').html(data[0].data);
            $(colummndef).find('.sampleresult').slideDown();
        }
        else
        {
            $(colummndef).find('.message-box .message').html(data[0].message);
            $(colummndef).find('.message-box').slideDown();
        }
        $(verifybtn).removeClass('disabled');
        
    }) .fail(function(){
        $(verifybtn).removeClass('disabled');                    
    });
})

$('#prosceedbtn').click(function () {
    var noofCols = $('#ColNoSliderinp').val();
    var scrapid = $('#scrapid').val();
    $.post( "/saveAndAddJob", {
        csrfmiddlewaretoken: "{{csrf_token}}",
        noofCols : noofCols,
        scrapid : scrapid
    })
    .done(function (data){
        if(data[0].status=='Success')
        {   
            window.location.replace("/myScrappers");
        }
        else
        {
            alert(data[0].Message);
        }
    })
});
