from django.conf.urls import patterns, include, url
from scrapper.views import *
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'scrappertrrdrr.views.home', name='home'),
    # url(r'^scrappertrrdrr/', include('scrappertrrdrr.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index),

    url(r'^NewScrapper$', addNewScrapper1),
    url(r'^addNewScrapper$', addNewScrapper2),

    url(r'^verifycolumn$', verifycolumn),
    url(r'^saveAndAddJob$', saveAndAddJob),

    url(r'^myScrappers$', myScrappers),
    #

    url(r'^sj/$', "index", name="jobs",),

    url(r'^script.js$', jscript),

    url(ur'^sj/(?P<key>.+)/(?P<OpType>\w+)/$', ScrappedDataOutput),


    (r'^accounts/login/$', 'django.contrib.auth.views.login'),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login'),
    (r'^accounts/password_change/$', 'django.contrib.auth.views.password_change'),
    (r'^myScrappers$', 'django.contrib.auth.views.password_change_done'),

    (r'^accounts/newUser/$', newUser),
    (r'^accounts/ContactUs/$', contactus),

)